document.addEventListener('DOMContentLoaded', ()=>
{
  const H1 = document.getElementsByClassName('heure')[0],
    H2 = document.getElementsByClassName('heure')[1],
    M1 = document.getElementsByClassName('minute')[0],
    M2 = document.getElementsByClassName('minute')[1],
    S1 = document.getElementsByClassName('seconde')[0],
    S2 = document.getElementsByClassName('seconde')[1],
    LINEHEIGTH = 10;

  function oui()
  {
    let date = new Date(),
      heure = date.getHours(),
      min = date.getMinutes(),
      sec = date.getSeconds();

    H1.style.setProperty('margin-top', 2 * LINEHEIGTH - Math.floor(heure / 10) * 2 * LINEHEIGTH + 'vmin');
    H2.style.setProperty('margin-top', 9 * LINEHEIGTH - Math.floor(heure % 10) * 2 * LINEHEIGTH + 'vmin');
    M1.style.setProperty('margin-top', 5 * LINEHEIGTH - Math.floor(min / 10) * 2 * LINEHEIGTH + 'vmin');
    M2.style.setProperty('margin-top', 9 * LINEHEIGTH - Math.floor(min % 10) * 2 * LINEHEIGTH + 'vmin');
    S1.style.setProperty('margin-top', 5 * LINEHEIGTH - Math.floor(sec / 10) * 2 * LINEHEIGTH + 'vmin');
    S2.style.setProperty('margin-top', 9 * LINEHEIGTH - Math.floor(sec % 10) * 2 * LINEHEIGTH + 'vmin');

    setTimeout(()=>
    {
      oui();
    }, 250)
  }

  oui();
});
